<!DOCTYPE html>
<html>
<head>
	<title>Pruebas</title>
</head>
<body>
<?php
class CabeceraPagina{
	private $titulo;
	private $ubicacion;
	private $fuente;
	private $fondo;

	public function __construct($tit,$ubi,$fuen,$fon)
	{
		$this->titulo=$tit;
		$this->ubicacion=$ubi;
		$this->fuente=$fuen;
		$this->fondo=$fon;
	}
	public function graficar()
	{
		echo '<div style="font-size:40px;text-align:'.$this->ubicacion.';color:'.$this->fuente.';background:'.$this->fondo.'">';
		echo $this->titulo;
		echo '</div>';
	}
}

$cabecera=new CabeceraPagina('El blog del programador','center','blue','yellow');
$cabecera->graficar();
?>
</body>
</html>