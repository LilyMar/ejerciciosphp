<!DOCTYPE html>
<html>
<head>
	<title>Pruebas</title>
</head>
<body>
<?php

class Tabla
{
	private $mat=array();
	private $cantFilas;
	private $cantColumnas;
	private $fondo=array();
	private $fuente=array();

	public function __construct($fi,$co)
	{
		$this->cantFilas=$fi;
		$this->cantColumnas=$co;
	}
	public function cargar($fila,$columna,$valor,$fon,$fuen)
	{
		$this->mat[$fila][$columna]=$valor;
		$this->fondo[$fila][$columna]=$fon;
		$this->fuente[$fila][$columna]=$fuen;
	}
	public function inicioTabla()
	{
		echo '<table border="1">';
	}
	public function inicioFila()
	{
		echo '<tr>';
	}
	public function mostrar($fi,$co)
	{
		echo '<td style="color:'.$this->fuente[$fi][$co].';background:'.$this->fondo[$fi][$co].'">'.$this->mat[$fi][$co].'</td>';
	}
	public function finFila()
	{
		echo '</tr>';
	}
	public function finTabla()
	{
		echo '</table>';
	}
	public function graficar()
	{
		$this->inicioTabla();
		for ($f=1;$f<=$this->cantFilas;$f++)
		{ 
			$this->inicioFila();
			for($c=1;$c<=$this->cantColumnas;$c++)
			{
				$this->mostrar($f,$c);
			}
			$this->finFila();
		}
		$this->finTabla();
	}
	
}

$tabla1=new Tabla(2,3);
$tabla1->cargar(1,1,"1","blue","green");
$tabla1->cargar(1,2,"2","blue","red");
$tabla1->cargar(1,3,"3","blue","green");
$tabla1->cargar(2,1,"4","blue","green");
$tabla1->cargar(2,2,"5","blue","green");
$tabla1->cargar(2,3,"6","blue","green");
$tabla1->graficar();

?>
</body>
</html>