<html>
<head>
<title>Prueba2</title>
</head>
<body>
<?php
    class Empleado
    {
      private $nombre;
      private $salario;

      public function inicializar($nom,$sal)
      {
                $this->nombre=$nom;
                $this->salario=$sal;
      }

      public function imprimir()
      {
                echo $this->nombre;
                if($this->salario>3000)
                   echo ' Debe pagar impuestos';
                else
                   echo ' No debe pagar impuestos';
                
                echo '</br>';
      }
    }
$emp1=new Empleado();
$emp1->inicializar('Jonathan','3500');
$emp1->imprimir();
$emp2=new Empleado();
$emp2->inicializar('lily','2000'); 
$emp2->imprimir(); 
?>
</body>
</html>
