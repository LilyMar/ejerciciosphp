<!DOCTYPE html>
<html>
<head>
	<title>Pruebas</title>
</head>
<body>
<?php
class CabeceraPagina{
	private $titulo;
	private $ubicacion;
	private $fondo;
	private $color;
	public function inicializar($tit,$ubi,$fon,$col)
	{
		$this->titulo=$tit;
		$this->ubicacion=$ubi;
		$this->fondo=$fon;
		$this->color=$col;
	}
	public function graficar()
	{
		echo '<div style="color:'.$this->color.';font-size:40px;text-align:'.$this->ubicacion.';background:'.$this->fondo.'">';
		echo $this->titulo;
		echo '</div>';
	}
}

$cabecera=new CabeceraPagina();
$cabecera->inicializar('El blog del programador','center','red','blue');
$cabecera->graficar();
?>
</body>
</html>